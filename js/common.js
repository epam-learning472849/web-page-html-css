function getFormInputDOM(formDOM, inputName) {
	var inputDOM = formDOM.getElementsByTagName("input")[inputName];
	if(typeof(inputDOM) !== 'undefined') {
		return inputDOM;
	}
	inputDOM = formDOM.getElementsByTagName("select")[inputName];
	if(typeof(inputDOM) !== 'undefined') {
		return inputDOM;
	}
	return null;
}

function resetValidation(selectDOM) {
	selectDOM.setCustomValidity('');
}